# Table of contents

* [Introduction](README.md)

## Affiliate Guide

* [THORName Guide](affiliate-guide/thorname-guide.md)

## Swap Guide

* [Quickstart Guide](swap-guide/quickstart-guide.md)
* [Fees and Wait Times](swap-guide/fees-and-wait-times.md)
* [Streaming Swaps](swap-guide/streaming-swaps.md)

## Saving Guide

* [Quickstart Guide](saving-guide/quickstart-guide.md)
* [Fees and Wait Times](saving-guide/fees-and-wait-times.md)

## Lending

* [Quick Start Guide](lending/quick-start-guide.md)

## Examples

* [Tutorials](examples/tutorials.md)
* [TypeScript (Web)](examples/typescript-web/README.md)
  * [Overview](examples/typescript-web/overview.md)
  * [Query Package](examples/typescript-web/query-package.md)
  * [AMM Package](examples/typescript-web/amm-package.md)
  * [Client Packages](examples/typescript-web/client-packages.md)
  * [Packages Breakdown](examples/typescript-web/packages-breakdown.md)
  * [Coding Guide](examples/typescript-web/coding-guide.md)

## Concepts

* [Connecting to THORChain](concepts/connecting-to-thorchain.md)
* [Querying THORChain](concepts/querying-thorchain.md)
* [Transaction Memos](concepts/memos.md)
* [Asset Notation](concepts/asset-notation.md)
* [Memo Length Reduction](concepts/memo-length-reduction.md)
* [Network Halts](concepts/network-halts.md)
* [Fees](concepts/fees.md)
* [Delays](concepts/delays.md)
* [Sending Transactions](concepts/sending-transactions.md)
* [Code Libraries](concepts/code-libraries.md)
* [Math](concepts/math.md)

## Aggregators

* [Aggregator Overview](aggregators/aggregator-overview.md)
* [Memos](aggregators/memos.md)
* [EVM Implementation](aggregators/evm-implementation.md)

## CLI

* [Overview](cli/overview.md)
* [Multisig](cli/multisig.md)
* [Offline Ledger Support](cli/offline-ledger-support.md)

## Protocol Development

* [Adding New Chains](protocol-development/adding-new-chains.md)
* [Chain Clients](protocol-development/chain-clients/README.md)
  * [UTXO](protocol-development/chain-clients/utxo.md)
  * [EVM Chains](protocol-development/chain-clients/evm-chains.md)
  * [BFT Chains](protocol-development/chain-clients/bft-chains.md)
* [ERC20 Tokens](protocol-development/erc20-tokens.md)

***

* [Dev Discord](https://discord.gg/7RRmc35UEG)
